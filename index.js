'use strict';

const debug = require('debug')('User');
const { NotAuthenticated } = require('Application');

function splitHeader(header) {
	if(typeof header === 'undefined')
		throw new NotAuthenticated();

	const [
		type,
		data
	] = header.split(' ');

	if(type != 'Basic')
		throw 'Only Basic authentication is supported';

	const [
		name,
		key
	] = Buffer.from(data, 'base64').toString('ascii').trim().split(':');

	return [
		name,
		key
	];
}

function merge(a, b) {
	if(typeof b === 'undefined')
		return Object.assign({}, a);

	const ret = {};
	for(const k in a) {
		ret[k] = Object.assign({}, a[k], b[k]);
	}
	for(const k in b) {
		ret[k] = Object.assign({}, a[k], b[k]);
	}
	return ret;
}

const defName = 'default';

class User {
	constructor(name = '') {
		this.name = name;
		this.key = '';
	}
	static async getDefPerm(pool) {
		if(!User.defPerm) {
			const {
				rows,
				rowCount
			} = await pool.query('SELECT permissions FROM users.permission WHERE name = $1::text', [defName]);
			if(rowCount != 1)
				throw 'Default Permission lookup returned multiple rows';

			User.defPerm = rows[0].permissions;
			debug('Default Permissions:');
			debug(User.defPerm);
		}

		return User.defPerm;
	}
	get isLoggedIn() {
		return this._logged_in;
	}
	async isValid(pool) {
		const {
			rows,
			rowCount
		} = await pool.query('SELECT name FROM users.users WHERE name = $1::text and hash IS NOT NULL', [this.name]);
		return rowCount === 1 && rows[0].name == this.name;
	}
	async checkKey(pool) {
		const {
			rows,
			rowCount
		} = await pool.query('SELECT name FROM users.sessions WHERE key = $1::text', [this.key]);
		if(rowCount > 1)
			throw 'Session lookup returned multiple rows';
		if(rowCount == 1 && rows[0].name != this.name)
			throw 'Session lookup returned a different user';
		debug(`User: ${this.name}`);
		this._logged_in = rowCount == 1;
		return this.isLoggedIn;
	}
	async getPermission(pool) {
		if(this.name !== '') {
			const {
				rows,
				rowCount
			} = await pool.query('SELECT permissions FROM users.permission WHERE name = $1::text', [this.name]);
			if(rowCount > 1)
				throw 'Permission lookup returned multiple rows';
			this.permission = merge(await User.getDefPerm(pool), rowCount == 1 ? rows[0].permissions : {});
		} else {
			this.permission = await User.getDefPerm(pool);
		}
		debug(`${this.name} permissions:`);
		debug(this.permission);
		return this.permission;
	}
	async check(ctx) {
		[this.name, this.key] = splitHeader(ctx.req.headers['authorization']);
		this.update_promise = ctx.pool.query(
			'SELECT users.update_lmod($1::text)',
			[this.key]
		).catch((e) => {
			debug(`update_lmod failed: ${e}`);
		});
		return this.checkKey(ctx.pool);
	}
}

module.exports = {
	User,
};
